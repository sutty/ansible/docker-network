Role Name
=========

Setup a Docker network with IPv6.

By using a /80 subnet on the same subnet of the Ekumen, containers on
different hosts can connect to each other.

Also reassigns addresses to running containers and restarts their
`nsupdate` services to update their DNS.

Requirements
------------

Hosts need to have an `ekumen` interface.

Role Variables
--------------

* `network`: Docker network name

Example Playbook
----------------

```yaml
- name: "Docker network"
  include_role:
    name: "sutty-docker-network"
  vars:
    network: "sutty"
```

Facts
-----

`ekumen_subnet` is the IPv6 subnet on the Ekumen for the current host.

License
-------

MIT-Antifa
